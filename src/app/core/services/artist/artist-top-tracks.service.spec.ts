import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { environment } from '../../../../environments/environment';
import { DataStates, ResultModel, TrackModel } from '../../models';

import { ArtistTopTracksService } from './artist-top-tracks.service';

const expectedTracks: TrackModel[] = [
  {
    duration: 326,
    explicit_content_cover: 0,
    explicit_content_lyrics: 1,
    explicit_lyrics: true,
    id: 1109731,
    link: 'https://www.deezer.com/track/1109731',
    md5_image: 'e2b36a9fda865cb2e9ed1476b6291a7d',
    preview: 'https://cdns-preview-1.dzcdn.net/stream/c-13039fed16a173733f227b0bec631034-12.mp3',
    rank: 952888,
    readable: true,
    title: 'Lose Yourself (From \'8 Mile\' Soundtrack)',
    title_short: 'Lose Yourself',
    title_version: '(From \'8 Mile\' Soundtrack)',
    type: 'track',
  },
];

describe('ArtistTopTracksService', () => {
  let controller: HttpTestingController;
  let service: ArtistTopTracksService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ]
    });
    service = TestBed.inject(ArtistTopTracksService);
    controller = TestBed.inject(HttpTestingController);
  });

  it('should be created with initial state', () => {
    expect(service).toBeTruthy();
    service.getState$.subscribe((state) => {
      expect(state).toEqual(DataStates.initial);
    });
  });

  it('should load a list tracks', () => {
    let tracks: TrackModel[] | undefined;

    const artistId = '13';
    const limit = 5;
    const expectedUrl = `${environment.endpoints.deezer}/artist/${artistId}/top?limit=${limit}`;

    service.load(artistId, limit);

    const request = controller.expectOne(expectedUrl);
    const response: ResultModel<TrackModel[]> = {
      data: expectedTracks,
    };

    request.flush(response);
    controller.verify();

    service.getList$.subscribe((result: TrackModel[]) => {
      tracks = result;
    });

    expect(tracks).toEqual(expectedTracks);
  });

  it('should have no tracks', () => {
    let tracks: TrackModel[] | undefined;

    const artistId = '999999999999999999999995';
    const limit = 5;
    const expectedUrl = `${environment.endpoints.deezer}/artist/${artistId}/top?limit=${limit}`;

    service.load(artistId, limit);

    const request = controller.expectOne(expectedUrl);
    const response: ResultModel<TrackModel[]> = {
      data: [],
    };

    request.flush(response);
    controller.verify();

    service.getList$.subscribe((result: TrackModel[]) => {
      tracks = result;
    });

    expect(tracks).toEqual([]);
  });
});
