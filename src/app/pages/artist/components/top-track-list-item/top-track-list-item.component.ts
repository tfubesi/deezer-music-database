import { Component, Input, OnInit } from '@angular/core';
import { TrackModel } from '../../../../core/models';

@Component({
  selector: 'app-top-track-list-item',
  templateUrl: './top-track-list-item.component.html',
  styleUrls: ['./top-track-list-item.component.scss']
})
export class TopTrackListItemComponent implements OnInit {

  @Input() track: TrackModel;
  @Input() trackIndex: number;

  get computedIndex(): number | null {
    if (typeof this.trackIndex !== 'number') {
      return null;
    }
    return this.trackIndex + 1;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
