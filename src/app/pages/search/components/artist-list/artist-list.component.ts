import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ArtistModel } from '../../../../core/models';

@Component({
  selector: 'app-artist-list',
  templateUrl: './artist-list.component.html',
  styleUrls: ['./artist-list.component.scss']
})
export class ArtistListComponent implements OnInit {

  @Input() artists: ArtistModel[];

  @Output() viewArtist = new EventEmitter<ArtistModel>();

  constructor() { }

  ngOnInit(): void {
  }

  onViewArtist(artist: ArtistModel): void {
    this.viewArtist.emit(artist);
  }

  trackById(index: number, artist: ArtistModel): number {
    return artist.id;
  }

}
