import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from './components/components.module';
import { SecondsToDurationPipe } from './pipes/seconds-to-duration.pipe';



@NgModule({
  declarations: [
    SecondsToDurationPipe
  ],
  imports: [
    CommonModule,
    ComponentsModule,
  ],
  exports: [
    SecondsToDurationPipe,
  ]
})
export class SharedModule { }
