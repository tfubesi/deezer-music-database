import {
  HttpClient,
  HttpErrorResponse,
  HttpParams,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject, Subscription } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { ArtistModel, DataStates, ResultModel } from '../../models';

@Injectable({
  providedIn: 'root',
})
export class ArtistListService {
  private list$: BehaviorSubject<ArtistModel[]>;
  get getList$(): Observable<ArtistModel[]> {
    return this.list$.asObservable();
  }

  private state$: BehaviorSubject<DataStates>;
  get getState$(): Observable<DataStates> {
    return this.state$.asObservable();
  }

  private error$: BehaviorSubject<string>;
  get getError$(): Observable<string> {
    return this.error$.asObservable();
  }

  private listSubscription?: Subscription;

  constructor(private http: HttpClient) {
    this.list$ = new BehaviorSubject<ArtistModel[]>([]);
    this.state$ = new BehaviorSubject<DataStates>(DataStates.initial);
    this.error$ = new BehaviorSubject<string>('');
  }

  load(search: string): void {
    this.state$.next(DataStates.loading);
    this.error$.next('');
    let httpParams = new HttpParams();
    if (!search) {
      this.reset();
      return;
    }
    httpParams = httpParams.set('q', search);

    this.listSubscription = this.http
      .get<ResultModel<ArtistModel[]>>(`${environment.endpoints.deezer}/search/artist`, {
        params: httpParams,
      })
      .subscribe({
        next: (response: ResultModel<ArtistModel[]>) => {
          if (response.data.length > 0) {
            this.list$.next(response.data);
            this.state$.next(DataStates.loaded);
            return;
          }
          this.state$.next(DataStates.failed);
          this.error$.next(`No artists found for: ${search}`);
        },
        error: (error: HttpErrorResponse) => {
          this.error$.next(error.message);
          this.state$.next(DataStates.failed);
        },
      });
  }

  reset(): void {
    this.error$.next('');
    this.list$.next([]);
    this.state$.next(DataStates.initial);
  }

  destroy(): void {
    this.list$.complete();
    this.error$.complete();
    this.state$.complete();
    this.listSubscription?.unsubscribe();
  }
}
