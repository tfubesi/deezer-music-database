import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

import { ArtistListService } from './artist-list.service';
import { ArtistModel, DataStates, ResultModel } from '../../models';
import { environment } from '../../../../environments/environment';

const expectedArtists: ArtistModel[] = [
  {
    id: 13,
    link: 'https://www.deezer.com/artist/13',
    name: 'Eminem',
    nb_album: 56,
    nb_fan: 15407030,
    picture: 'https://api.deezer.com/artist/13/image',
    picture_big:
      'https://e-cdns-images.dzcdn.net/images/artist/19cc38f9d69b352f718782e7a22f9c32/500x500-000000-80-0-0.jpg',
    picture_medium:
      'https://e-cdns-images.dzcdn.net/images/artist/19cc38f9d69b352f718782e7a22f9c32/250x250-000000-80-0-0.jpg',
    picture_small:
      'https://e-cdns-images.dzcdn.net/images/artist/19cc38f9d69b352f718782e7a22f9c32/56x56-000000-80-0-0.jpg',
    picture_xl:
      'https://e-cdns-images.dzcdn.net/images/artist/19cc38f9d69b352f718782e7a22f9c32/1000x1000-000000-80-0-0.jpg',
    radio: true,
    share: '',
    tracklist: 'https://api.deezer.com/artist/13/top?limit=50',
    type: 'artist',
  },
];

describe('ArtistListService', () => {
  let controller: HttpTestingController;
  let service: ArtistListService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(ArtistListService);
    controller = TestBed.inject(HttpTestingController);
  });

  it('should be created with initial state', () => {
    expect(service).toBeTruthy();
    service.getState$.subscribe((state) => {
      expect(state).toEqual(DataStates.initial);
    });
  });

  it('should search and load a list of artists', () => {
    let artists: ArtistModel[] | undefined;

    const query = 'eminem';
    const expectedUrl = `${environment.endpoints.deezer}/search/artist?q=${query}`;

    service.load(query);

    const request = controller.expectOne(expectedUrl);
    const response: ResultModel<ArtistModel[]> = {
      data: expectedArtists,
    };

    request.flush(response);
    controller.verify();

    service.getList$.subscribe((result: ArtistModel[]) => {
      artists = result;
    });

    expect(artists).toEqual(expectedArtists);
  });

  it('should have no artists', () => {
    let artists: ArtistModel[] | undefined;

    const query = 'sss99999999999999999';

    const expectedUrl = `${environment.endpoints.deezer}/search/artist?q=${query}`;

    service.load(query);

    const request = controller.expectOne(expectedUrl);
    const response: ResultModel<ArtistModel[]> = {
      data: [],
    };

    request.flush(response);
    controller.verify();

    service.getList$.subscribe((result: ArtistModel[]) => {
      artists = result;
    });

    expect(artists).toEqual([]);
  });

});
