import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArtistRoutingModule } from './artist-routing.module';
import { ArtistShellComponent } from './containers/artist-shell/artist-shell.component';
import { ArtistProfileComponent } from './components/artist-profile/artist-profile.component';
import { AlbumListComponent } from './components/album-list/album-list.component';
import { AlbumListItemComponent } from './components/album-list-item/album-list-item.component';
import { TopTrackListComponent } from './components/top-track-list/top-track-list.component';
import { TopTrackListItemComponent } from './components/top-track-list-item/top-track-list-item.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    ArtistShellComponent,
    ArtistProfileComponent,
    AlbumListComponent,
    AlbumListItemComponent,
    TopTrackListComponent,
    TopTrackListItemComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ArtistRoutingModule
  ]
})
export class ArtistModule { }
