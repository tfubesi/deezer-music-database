import {
  HttpClient,
  HttpParams,
  HttpErrorResponse,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { AlbumModel, DataStates, ResultModel } from '../../models';

@Injectable({
  providedIn: 'root',
})
export class ArtistAlbumsService {
  private list$: BehaviorSubject<AlbumModel[]>;
  get getList$(): Observable<AlbumModel[]> {
    return this.list$.asObservable();
  }

  get isLoading$(): Observable<boolean> {
    return this.getState$.pipe(map((response) => response === DataStates.loading));
  }

  private state$: BehaviorSubject<DataStates>;
  get getState$(): Observable<DataStates> {
    return this.state$.asObservable();
  }

  private error$: Subject<string>;
  get getError$(): Observable<string> {
    return this.error$.asObservable();
  }

  private listSubscription?: Subscription;

  constructor(private http: HttpClient) {
    this.list$ = new BehaviorSubject<AlbumModel[]>([]);
    this.state$ = new BehaviorSubject<DataStates>(DataStates.initial);
    this.error$ = new Subject<string>();
  }

  load(artistId: string, limit: number): void {
    this.state$.next(DataStates.loading);
    this.error$.next('');
    let httpParams = new HttpParams();
    if (limit) {
      httpParams = httpParams.set('limit', limit.toString());
    }

    this.listSubscription = this.http
      .get<ResultModel<AlbumModel[]>>(
        `${environment.endpoints.deezer}/artist/${artistId}/albums`,
        {
          params: httpParams,
        }
      )
      .subscribe({
        next: (response: ResultModel<AlbumModel[]>) => {
          if (response.data.length > 0) {
            this.list$.next(response.data);
            this.state$.next(DataStates.loaded);
            return;
          }
          this.state$.next(DataStates.failed);
          this.error$.next(`No albums found for this artist`);
        },
        error: (error: HttpErrorResponse) => {
          this.error$.next(error.message);
          this.state$.next(DataStates.failed);
        },
      });
  }

  reset(): void {
    this.error$.next(undefined);
    this.list$.next([]);
    this.state$.next(DataStates.initial);
  }

  destroy(): void {
    this.list$.complete();
    this.error$.complete();
    this.state$.complete();
    this.listSubscription?.unsubscribe();
  }
}
