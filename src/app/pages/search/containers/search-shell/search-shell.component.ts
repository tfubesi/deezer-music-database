import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ArtistModel, DataStates } from '../../../../core/models';
import { ArtistListService } from '../../../../core/services';

@Component({
  selector: 'app-search-shell',
  templateUrl: './search-shell.component.html',
  styleUrls: ['./search-shell.component.scss'],
})
export class SearchShellComponent implements OnInit {
  artists$: Observable<ArtistModel[]>;

  searchState$: Observable<DataStates>;
  searchError$: Observable<string>;
  searchStates: typeof DataStates;

  constructor(
    private router: Router,
    private artistListService: ArtistListService
  ) {
    this.searchStates = DataStates;
  }

  ngOnInit(): void {
    this.artists$ = this.artistListService.getList$;
    this.searchState$ = this.artistListService.getState$;
    this.searchError$ = this.artistListService.getError$;
  }

  onViewArtist(artist: ArtistModel): void {
    this.router.navigate(['artist', artist.id]);
  }
}
