import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { ArtistModel, DataStates, DeezerException } from '../../models';

@Injectable({
  providedIn: 'root',
})
export class ArtistService {
  private artist$: BehaviorSubject<ArtistModel | null>;
  get getArtist$(): Observable<ArtistModel | null> {
    return this.artist$.asObservable();
  }

  private state$: BehaviorSubject<DataStates>;
  get getState$(): Observable<DataStates> {
    return this.state$.asObservable();
  }

  get isLoading$(): Observable<boolean> {
    return this.state$.pipe(map((response) => response === DataStates.loading));
  }

  private error$: BehaviorSubject<string>;
  get getError$(): Observable<string> {
    return this.error$.asObservable();
  }

  private artistSubscription?: Subscription;

  constructor(private http: HttpClient) {
    this.artist$ = new BehaviorSubject<ArtistModel | null>(null);
    this.state$ = new BehaviorSubject<DataStates>(DataStates.initial);
    this.error$ = new BehaviorSubject<string>('');
  }

  loadById(artistId: string): void {
    this.state$.next(DataStates.loading);
    this.error$.next('');

    this.artistSubscription = this.http
      .get<ArtistModel>(`${environment.endpoints.deezer}/artist/${artistId}`)
      .subscribe({
        next: (response: ArtistModel | DeezerException) => {
          if (response && response.hasOwnProperty('error')) {
            const result: DeezerException = response as DeezerException;
            this.state$.next(DataStates.failed);
            this.error$.next(result.error.message);
            return;
          }
          this.artist$.next(response as ArtistModel);
          this.state$.next(DataStates.loaded);
        },
        error: (error: HttpErrorResponse) => {
          this.error$.next(error.message);
          this.state$.next(DataStates.failed);
        },
      });
  }

  reset(): void {
    this.error$.next('');
    this.artist$.next(null);
    this.state$.next(DataStates.initial);
  }

  destroy(): void {
    this.artist$.complete();
    this.error$.complete();
    this.state$.complete();
    this.artistSubscription?.unsubscribe();
  }
}
