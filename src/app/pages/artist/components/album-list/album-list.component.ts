import { Component, Input, OnInit } from '@angular/core';
import { AlbumModel } from '../../../../core/models';

@Component({
  selector: 'app-album-list',
  templateUrl: './album-list.component.html',
  styleUrls: ['./album-list.component.scss']
})
export class AlbumListComponent implements OnInit {

  @Input() albumList: AlbumModel[] | null;
  @Input() loading: boolean | null;
  @Input() error: string | null;

  get hasData(): boolean {
    return Array.isArray(this.albumList) && this.albumList.length > 0;
  }

  constructor() { }

  ngOnInit(): void {
  }

  trackById(index: number, album: AlbumModel): number {
    return album.id;
  }

}
