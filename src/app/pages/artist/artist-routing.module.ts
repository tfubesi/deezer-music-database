import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArtistShellComponent } from './containers/artist-shell/artist-shell.component';

const routes: Routes = [
  {
    path: '',
    component: ArtistShellComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArtistRoutingModule { }
