import { EnvironmentConfig } from "./environment.interface";

export const environment: EnvironmentConfig = {
  production: true,
  endpoints: {
    deezer: 'https://cors-anywhere.herokuapp.com/corsdemo/https://api.deezer.com',
  },
  settings: {
    topTracksLimit: 5,
  }
};
