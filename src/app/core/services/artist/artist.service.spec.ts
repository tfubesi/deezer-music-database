import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { environment } from '../../../../environments/environment';
import { ArtistModel, DataStates } from '../../models';

import { ArtistService } from './artist.service';

const expectedArtist: ArtistModel = {
  id: 13,
  link: 'https://www.deezer.com/artist/13',
  name: 'Eminem',
  nb_album: 56,
  nb_fan: 15407030,
  picture: 'https://api.deezer.com/artist/13/image',
  picture_big:
    'https://e-cdns-images.dzcdn.net/images/artist/19cc38f9d69b352f718782e7a22f9c32/500x500-000000-80-0-0.jpg',
  picture_medium:
    'https://e-cdns-images.dzcdn.net/images/artist/19cc38f9d69b352f718782e7a22f9c32/250x250-000000-80-0-0.jpg',
  picture_small:
    'https://e-cdns-images.dzcdn.net/images/artist/19cc38f9d69b352f718782e7a22f9c32/56x56-000000-80-0-0.jpg',
  picture_xl:
    'https://e-cdns-images.dzcdn.net/images/artist/19cc38f9d69b352f718782e7a22f9c32/1000x1000-000000-80-0-0.jpg',
  radio: true,
  share: '',
  tracklist: 'https://api.deezer.com/artist/13/top?limit=50',
  type: 'artist',
};

describe('ArtistService', () => {
  let controller: HttpTestingController;
  let service: ArtistService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(ArtistService);
    controller = TestBed.inject(HttpTestingController);
  });

  it('should be created with initial state', () => {
    expect(service).toBeTruthy();
    service.getState$.subscribe((state) => {
      expect(state).toEqual(DataStates.initial);
    });
  });

  it('should load an artist', () => {
    let artist: ArtistModel | undefined | null;

    const artistId = '13';
    const expectedUrl = `${environment.endpoints.deezer}/artist/${artistId}`;

    service.loadById(artistId);

    const request = controller.expectOne(expectedUrl);
    const response: ArtistModel = expectedArtist;

    request.flush(response);
    controller.verify();

    service.getArtist$.subscribe((result: ArtistModel | null) => {
      artist = result;
    });

    expect(artist).toEqual(expectedArtist);
  });

  it('should not find an artist', () => {
    let artist: ArtistModel | undefined | null;

    const artistId = '13';
    const expectedUrl = `${environment.endpoints.deezer}/artist/${artistId}`;

    service.loadById(artistId);

    const request = controller.expectOne(expectedUrl);
    const response: Partial<ArtistModel | null> = null;

    request.flush(response);
    controller.verify();

    service.getArtist$.subscribe((result: ArtistModel | null) => {
      artist = result;
    });

    expect(artist).toEqual(null);
  });
});
