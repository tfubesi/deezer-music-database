import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { AlbumModel, ArtistModel, TrackModel } from '../../../../core/models';
import { ArtistAlbumsService, ArtistService, ArtistTopTracksService } from '../../../../core/services';

@Component({
  selector: 'app-artist-shell',
  templateUrl: './artist-shell.component.html',
  styleUrls: ['./artist-shell.component.scss']
})
export class ArtistShellComponent implements OnInit, OnDestroy {

  artist$: Observable<ArtistModel | null>;
  artistLoading$: Observable<boolean>;
  artistError$: Observable<string>;

  albumList$: Observable<AlbumModel[]>;
  albumListLoading$: Observable<boolean>;
  albumListError$: Observable<string>;

  topTrackList$: Observable<TrackModel[]>;
  topTrackListLoading$: Observable<boolean>;
  topTrackListError$: Observable<string>;

  private routeSubscription?: Subscription;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private artistAlbumsService: ArtistAlbumsService,
    private artistService: ArtistService,
    private artistTopTracksService: ArtistTopTracksService,
  ) { }

  ngOnInit(): void {

    this.artist$ = this.artistService.getArtist$;
    this.artistLoading$ = this.artistService.isLoading$;
    this.artistError$ = this.artistService.getError$;

    this.albumList$ = this.artistAlbumsService.getList$;
    this.albumListLoading$ = this.artistAlbumsService.isLoading$;
    this.albumListError$ = this.artistAlbumsService.getError$;

    this.topTrackList$ = this.artistTopTracksService.getList$;
    this.topTrackListLoading$ = this.artistTopTracksService.isLoading$;
    this.topTrackListError$ = this.artistTopTracksService.getError$;

    this.routeSubscription = this.activatedRoute.paramMap.subscribe((params) => {
      if (!params.has('id')) {
        this.router.navigate(['/']);
        return;
      }
      const artistId = params.get('id');
      if (!artistId) {
        this.router.navigate(['/']);
        return;
      }
      this.artistService.loadById(artistId);
      this.artistAlbumsService.load(artistId, 100);
      this.artistTopTracksService.load(artistId, environment.settings.topTracksLimit);
    });
  }

  ngOnDestroy(): void {
    this.artistService.reset();
    this.artistAlbumsService.reset();
    this.artistTopTracksService.reset();
    this.routeSubscription?.unsubscribe();
  }
}
