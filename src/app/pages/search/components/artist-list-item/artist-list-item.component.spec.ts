import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtistListItemComponent } from './artist-list-item.component';

describe('ArtistListItemComponent', () => {
  let component: ArtistListItemComponent;
  let fixture: ComponentFixture<ArtistListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArtistListItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistListItemComponent);
    component = fixture.componentInstance;
    component.artist = {
      id: 13,
      link: 'https://www.deezer.com/artist/13',
      name: 'Eminem',
      nb_album: 56,
      nb_fan: 15407030,
      picture: 'https://api.deezer.com/artist/13/image',
      picture_big: 'https://e-cdns-images.dzcdn.net/images/artist/19cc38f9d69b352f718782e7a22f9c32/500x500-000000-80-0-0.jpg',
      picture_medium: 'https://e-cdns-images.dzcdn.net/images/artist/19cc38f9d69b352f718782e7a22f9c32/250x250-000000-80-0-0.jpg',
      picture_small: 'https://e-cdns-images.dzcdn.net/images/artist/19cc38f9d69b352f718782e7a22f9c32/56x56-000000-80-0-0.jpg',
      picture_xl: 'https://e-cdns-images.dzcdn.net/images/artist/19cc38f9d69b352f718782e7a22f9c32/1000x1000-000000-80-0-0.jpg',
      radio: true,
      share: 'https://www.deezer.com/artist/13?utm_source=deezer&utm_content=artist-13&utm_term=0_1654438887&utm_medium=android',
      tracklist: 'https://api.deezer.com/artist/13/top?limit=50',
      type: 'artist',
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
