export interface ResultModel<T> {
  data: T;
  total?: number;
  prev?: string;
  next?: string;
}
