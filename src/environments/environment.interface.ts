export interface EnvironmentConfig {
  production: boolean;
  endpoints: {
    deezer: string;
  };
  settings: {
    topTracksLimit: number;
  };
}
