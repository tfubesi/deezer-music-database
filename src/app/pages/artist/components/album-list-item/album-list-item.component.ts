import { Component, Input, OnInit } from '@angular/core';
import { AlbumModel } from '../../../../core/models';

@Component({
  selector: 'app-album-list-item',
  templateUrl: './album-list-item.component.html',
  styleUrls: ['./album-list-item.component.scss']
})
export class AlbumListItemComponent implements OnInit {

  @Input() album: AlbumModel;

  constructor() { }

  ngOnInit(): void {
  }

}
