import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AlbumListItemComponent } from '../../components/album-list-item/album-list-item.component';
import { AlbumListComponent } from '../../components/album-list/album-list.component';
import { ArtistProfileComponent } from '../../components/artist-profile/artist-profile.component';
import { TopTrackListItemComponent } from '../../components/top-track-list-item/top-track-list-item.component';
import { TopTrackListComponent } from '../../components/top-track-list/top-track-list.component';

import { ArtistShellComponent } from './artist-shell.component';

describe('ArtistShellComponent', () => {
  let component: ArtistShellComponent;
  let fixture: ComponentFixture<ArtistShellComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ArtistShellComponent,
        ArtistShellComponent,
        ArtistProfileComponent,
        AlbumListComponent,
        AlbumListItemComponent,
        TopTrackListComponent,
        TopTrackListItemComponent,
      ],
      imports: [RouterTestingModule, HttpClientTestingModule],
      providers: [],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
