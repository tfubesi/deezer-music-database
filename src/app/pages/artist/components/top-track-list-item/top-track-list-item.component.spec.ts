import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SecondsToDurationPipe } from '../../../../shared/pipes/seconds-to-duration.pipe';

import { TopTrackListItemComponent } from './top-track-list-item.component';

describe('TopTrackListItemComponent', () => {
  let component: TopTrackListItemComponent;
  let fixture: ComponentFixture<TopTrackListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopTrackListItemComponent, SecondsToDurationPipe ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopTrackListItemComponent);
    component = fixture.componentInstance;
    component.track = {
      duration: 210,
      explicit_content_cover: 1,
      explicit_content_lyrics: 1,
      explicit_lyrics: true,
      id: 854914322,
      link: 'https://www.deezer.com/track/854914322',
      md5_image: '4d00a7848dc8af475973ff1761ad828d',
      preview: 'https://cdns-preview-d.dzcdn.net/stream/c-d5a91f3cf9c2b399c9734223623a3c67-6.mp3',
      rank: 913535,
      readable: true,
      title: 'Godzilla',
      title_short: 'Godzilla',
      title_version: '',
      type: 'track',
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
