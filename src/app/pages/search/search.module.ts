import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchRoutingModule } from './search-routing.module';
import { SearchShellComponent } from './containers/search-shell/search-shell.component';
import { ArtistListComponent } from './components/artist-list/artist-list.component';
import { ArtistListItemComponent } from './components/artist-list-item/artist-list-item.component';


@NgModule({
  declarations: [
    SearchShellComponent,
    ArtistListComponent,
    ArtistListItemComponent
  ],
  imports: [
    CommonModule,
    SearchRoutingModule
  ]
})
export class SearchModule { }
