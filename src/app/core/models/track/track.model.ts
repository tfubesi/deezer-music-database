import { AlbumModel, ArtistModel, ContributorModel } from '..';

export interface TrackModel {
    id: number;
    readable: boolean;
    title: string;
    title_short: string;
    title_version: string;
    link: string;
    duration: number;
    rank: number;
    explicit_lyrics: boolean;
    explicit_content_lyrics: number;
    explicit_content_cover: number;
    preview: string;
    contributors?: ContributorModel[];
    md5_image: string;
    artist?: ArtistModel;
    album?: AlbumModel;
    type: string;
}
