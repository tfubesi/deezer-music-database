export enum DataStates {
  initial,
  loading,
  loaded,
  failed,
}
