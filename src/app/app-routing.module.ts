import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '', loadChildren: () => import('./pages/search/search.module').then(m => m.SearchModule)
  },
  {
    path: 'artist/:id', loadChildren: () => import('./pages/artist/artist.module').then(m => m.ArtistModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'top',
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
