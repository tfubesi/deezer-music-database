import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ArtistModel } from '../../../../core/models';

@Component({
  selector: 'app-artist-list-item',
  templateUrl: './artist-list-item.component.html',
  styleUrls: ['./artist-list-item.component.scss']
})
export class ArtistListItemComponent implements OnInit {

  @Input() artist: ArtistModel;

  @Output() viewArtist = new EventEmitter<ArtistModel>();

  constructor() { }

  ngOnInit(): void {
  }

  onViewArtist(artist: ArtistModel): void {
    this.viewArtist.emit(artist);
  }

}
