import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumListItemComponent } from './album-list-item.component';

describe('AlbumListItemComponent', () => {
  let component: AlbumListItemComponent;
  let fixture: ComponentFixture<AlbumListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlbumListItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumListItemComponent);
    component = fixture.componentInstance;
    component.album = {
      cover: 'https://api.deezer.com/album/72000342/image',
      cover_big: 'https://e-cdns-images.dzcdn.net/images/cover/bf74fc764097630ba58782ae79cfbee6/500x500-000000-80-0-0.jpg',
      cover_medium: 'https://e-cdns-images.dzcdn.net/images/cover/bf74fc764097630ba58782ae79cfbee6/250x250-000000-80-0-0.jpg',
      cover_small: 'https://e-cdns-images.dzcdn.net/images/cover/bf74fc764097630ba58782ae79cfbee6/56x56-000000-80-0-0.jpg',
      cover_xl: 'https://e-cdns-images.dzcdn.net/images/cover/bf74fc764097630ba58782ae79cfbee6/1000x1000-000000-80-0-0.jpg',
      explicit_lyrics: true,
      fans: 334888,
      genre_id: 116,
      id: 72000342,
      link: 'https://www.deezer.com/album/72000342',
      md5_image: 'bf74fc764097630ba58782ae79cfbee6',
      record_type: 'album',
      release_date: '2018-08-31',
      title: 'Kamikaze',
      tracklist: 'https://api.deezer.com/album/72000342/tracks',
      type: 'album',
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
