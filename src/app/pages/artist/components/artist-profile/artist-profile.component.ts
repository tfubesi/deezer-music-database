import { Component, Input, OnInit } from '@angular/core';
import { ArtistModel } from '../../../../core/models';

@Component({
  selector: 'app-artist-profile',
  templateUrl: './artist-profile.component.html',
  styleUrls: ['./artist-profile.component.scss']
})
export class ArtistProfileComponent implements OnInit {

  @Input() artist: ArtistModel | null;
  @Input() loading: boolean | null;
  @Input() error: string | null;

  get hasData(): boolean {
    return this.artist !== null || this.artist !== undefined;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
