export interface ArtistModel {
  id: number;
  name: string;
  link: string;
  picture: string;
  picture_small: string;
  picture_medium: string;
  picture_big: string;
  picture_xl: string;
  share?: string;
  tracklist: string;
  nb_album: number;
  nb_fan: number;
  radio: boolean;
  type: string;
}
