import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { DataStates, ResultModel, TrackModel } from '../../models';

@Injectable({
  providedIn: 'root'
})
export class ArtistTopTracksService {
  private list$: BehaviorSubject<TrackModel[]>;
  get getList$(): Observable<TrackModel[]> {
    return this.list$.asObservable();
  }

  private state$: BehaviorSubject<DataStates>;
  get getState$(): Observable<DataStates> {
    return this.state$.asObservable();
  }

  get isLoading$(): Observable<boolean> {
    return this.state$.pipe(map(response => response === DataStates.loading));
  }

  private error$: BehaviorSubject<string>;
  get getError$(): Observable<string> {
    return this.error$.asObservable();
  }

  private listSubscription?: Subscription;

  constructor(private http: HttpClient) {
    this.list$ = new BehaviorSubject<TrackModel[]>([]);
    this.state$ = new BehaviorSubject<DataStates>(DataStates.initial);
    this.error$ = new BehaviorSubject<string>('');
  }

  load(artistId: string, limit: number): void {
    this.state$.next(DataStates.loading);
    this.error$.next('');

    let httpParams = new HttpParams();
    if (limit) {
      httpParams = httpParams.set('limit', limit.toString());
    }

    this.listSubscription = this.http
      .get<ResultModel<TrackModel[]>>(`${environment.endpoints.deezer}/artist/${artistId}/top`, {
        params: httpParams,
      })
      .subscribe({
        next: (response: ResultModel<TrackModel[]>) => {
          if (response.data.length > 0) {
            this.list$.next(response.data);
            this.state$.next(DataStates.loaded);
            return;
          }
          this.state$.next(DataStates.failed);
          this.error$.next(`No tracks found for this artist`);
        },
        error: (error: HttpErrorResponse) => {
          this.error$.next(error.message);
          this.state$.next(DataStates.failed);
        },
      });
  }

  reset(): void {
    this.error$.next('');
    this.list$.next([]);
    this.state$.next(DataStates.initial);
  }

  destroy(): void {
    this.list$.complete();
    this.error$.complete();
    this.state$.complete();
    this.listSubscription?.unsubscribe();
  }
}
