import { DeezerExceptionBody } from '..';

export interface DeezerException {
  error: DeezerExceptionBody;
}
