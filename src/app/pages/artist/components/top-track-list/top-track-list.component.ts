import { Component, Input, OnInit } from '@angular/core';
import { TrackModel } from '../../../../core/models';

@Component({
  selector: 'app-top-track-list',
  templateUrl: './top-track-list.component.html',
  styleUrls: ['./top-track-list.component.scss']
})
export class TopTrackListComponent implements OnInit {

  @Input() trackList: TrackModel[] | null;
  @Input() loading: boolean | null;
  @Input() error: string | null;

  get hasData(): boolean {
    return Array.isArray(this.trackList) && this.trackList.length > 0;
  }

  constructor() { }

  ngOnInit(): void {
  }

  trackById(index: number, track: TrackModel): number {
    return track.id;
  }

}
