export interface DeezerExceptionBody {
  type: string;
  message: string;
  code: number;
}
