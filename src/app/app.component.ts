import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Subject, Subscription } from 'rxjs';
import { ArtistListService } from './core/services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  private search$: Subject<string>;

  private searchSubScription?: Subscription;
  constructor(private artistListService: ArtistListService, private router: Router) {
    this.search$ = new Subject();
  }

  ngOnInit(): void {
    this.searchSubScription = this.search$.subscribe((response: string) => {
      this.artistListService.load(response);
    });
  }

  ngOnDestroy(): void {
    this.search$.complete();
    this.searchSubScription?.unsubscribe();
  }

  onSearchChanged(searchValue: string): void {
    this.search$.next(searchValue);
    if (this.router.url !== '/') {
      this.router.navigate(['/']);
    }
  }
}
