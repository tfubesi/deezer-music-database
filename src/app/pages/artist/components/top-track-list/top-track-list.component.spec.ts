import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SecondsToDurationPipe } from '../../../../shared/pipes/seconds-to-duration.pipe';

import { TopTrackListComponent } from './top-track-list.component';

describe('TopTrackListComponent', () => {
  let component: TopTrackListComponent;
  let fixture: ComponentFixture<TopTrackListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopTrackListComponent, SecondsToDurationPipe, ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopTrackListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
