import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  @Output() searchChanged = new EventEmitter<string>();

  searchForm: FormGroup;

  private searchSubscription?: Subscription;

  constructor(private fb: FormBuilder) {
    this.searchForm = this.fb.group({
      search: [''],
    });
  }

  ngOnInit(): void {
    this.searchSubscription = this.searchForm.get('search')?.valueChanges.pipe(
      distinctUntilChanged(),
      debounceTime(500),
    ).subscribe((response: string) => {
      this.searchChanged.emit(response);
    });
  }

  ngOnDestroy(): void {
    this.searchSubscription?.unsubscribe();
  }

}
