import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { take } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { AlbumModel, DataStates, ResultModel } from '../../models';

import { ArtistAlbumsService } from './artist-albums.service';

const expectedAlbums: AlbumModel[] = [
  {
    cover: 'https://api.deezer.com/album/127270232/image',
    cover_big:
      'https://e-cdns-images.dzcdn.net/images/cover/4d00a7848dc8af475973ff1761ad828d/500x500-000000-80-0-0.jpg',
    cover_medium:
      'https://e-cdns-images.dzcdn.net/images/cover/4d00a7848dc8af475973ff1761ad828d/250x250-000000-80-0-0.jpg',
    cover_small:
      'https://e-cdns-images.dzcdn.net/images/cover/4d00a7848dc8af475973ff1761ad828d/56x56-000000-80-0-0.jpg',
    cover_xl:
      'https://e-cdns-images.dzcdn.net/images/cover/4d00a7848dc8af475973ff1761ad828d/1000x1000-000000-80-0-0.jpg',
    explicit_lyrics: true,
    fans: 217219,
    genre_id: 116,
    id: 127270232,
    link: 'https://www.deezer.com/album/127270232',
    md5_image: '4d00a7848dc8af475973ff1761ad828d',
    record_type: 'album',
    release_date: '2020-01-17',
    title: 'Music To Be Murdered By',
    tracklist: 'https://api.deezer.com/album/127270232/tracks',
    type: 'album',
  },
];

describe('ArtistAlbumsService', () => {
  let controller: HttpTestingController;
  let service: ArtistAlbumsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(ArtistAlbumsService);
    controller = TestBed.inject(HttpTestingController);
  });

  it('should be created with initial state', () => {
    expect(service).toBeTruthy();
    service.getState$.subscribe((state) => {
      expect(state).toEqual(DataStates.initial);
    });
  });

  it('should load albums', () => {
    let albums: AlbumModel[] | undefined;

    const artistId = '13';
    const limit = 5;

    const expectedUrl = `${
      environment.endpoints.deezer
    }/artist/${artistId}/albums?limit=${limit.toString()}`;

    service.load(artistId, limit);

    const request = controller.expectOne(expectedUrl);

    const response: ResultModel<AlbumModel[]> = {
      data: expectedAlbums,
    };

    request.flush(response);

    controller.verify();

    service.getList$.subscribe((result: AlbumModel[]) => {
      albums = result;
    });

    expect(albums).toEqual(expectedAlbums);

  });

  it('should have no albums', () => {
    let albums: AlbumModel[] | undefined;

    const artistId = '9999999999999999999';
    const limit = 5;

    const expectedUrl = `${
      environment.endpoints.deezer
    }/artist/${artistId}/albums?limit=${limit.toString()}`;

    service.load(artistId, limit);

    const request = controller.expectOne(expectedUrl);
    const response: ResultModel<AlbumModel[]> = {
      data: [],
    };

    request.flush(response);
    controller.verify();

    service.getList$.subscribe((result) => {
      albums = result;
    });

    expect(albums).toEqual([]);
  });

});
