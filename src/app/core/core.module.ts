import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArtistAlbumsService, ArtistListService, ArtistTopTracksService } from './services';



@NgModule({
  declarations: [],
  providers: [
    ArtistAlbumsService,
    ArtistListService,
    ArtistTopTracksService
  ],
  imports: [
    CommonModule
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule?: CoreModule) {
    if (parentModule) {
      throw new Error('CorModule has already been loaded.');
    }
  }
}
