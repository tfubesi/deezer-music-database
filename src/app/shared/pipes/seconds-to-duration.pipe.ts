import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'secondsToDuration'
})
export class SecondsToDurationPipe implements PipeTransform {

  transform(value: number, ...args: unknown[]): unknown {
    if (!value) {
      return null;
    }
    const duration = new Date(value * 1000).toISOString().slice(14, 19);
    return duration;
  }

}
