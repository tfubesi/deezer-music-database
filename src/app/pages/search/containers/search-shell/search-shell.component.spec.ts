import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ArtistListItemComponent } from '../../components/artist-list-item/artist-list-item.component';
import { ArtistListComponent } from '../../components/artist-list/artist-list.component';

import { SearchShellComponent } from './search-shell.component';

describe('SearchShellComponent', () => {
  let component: SearchShellComponent;
  let fixture: ComponentFixture<SearchShellComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        SearchShellComponent,
        SearchShellComponent,
        ArtistListComponent,
        ArtistListItemComponent,
      ],
      imports: [RouterTestingModule, HttpClientTestingModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
